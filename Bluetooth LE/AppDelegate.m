//
//  AppDelegate.m
//  Bluetooth LE
//
//  Created by Jens Willy Johannsen on 30-04-12.
//  Copyright (c) 2012 Greener Pastures. All rights reserved.
//

#import "AppDelegate.h"
#import "CBUUID+Utils.h"
#import "MainViewController.h"
#import "PageListViewController.h"
#import "Bluetooth_LE-Swift.h"

@implementation AppDelegate

@synthesize window = _window, preferredDeviceUUID;
@synthesize orientation = _orientation;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
	NSArray *centralManagers = launchOptions[ UIApplicationLaunchOptionsBluetoothCentralsKey ];
	for( NSString *centralIdentifier in centralManagers )
		if( [centralIdentifier isEqualToString:@"MultiRemoteCentralManager" ] )
			DEBUG_LOG( @"Central manager restored." );

	// Set split view controller's delegate (can't do this in IB for some reason)
	if( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
	{
		[(UISplitViewController*)_window.rootViewController setDelegate:[self mainViewController]];
	}
	
	[self.window makeKeyAndVisible];
	[self setAppearance];

//	dispatch_async(dispatch_get_main_queue(), ^{
		[self showSplash:UIInterfaceOrientationPortrait];
//	});

    return YES;
}
	
- (void)showSplash:(UIInterfaceOrientation)orientation
{
	// Create image view with image that match device and orientation
	UIImage *image;
	CGAffineTransform transform = CGAffineTransformIdentity;
	CGPoint origin = CGPointZero;
	
	if( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
	{
		// iPad: which orientation?
		orientation = [[UIApplication sharedApplication] statusBarOrientation];
		if( UIInterfaceOrientationIsLandscape( orientation ))
		{
			image = [UIImage imageNamed:@"Default-Landscape.png"];
		}
		else
		{
			image = [UIImage imageNamed:@"Default-Portrait.png"];
		}
	}
	else
	{
		// iPhone 4 or 5?
		if( [UIScreen mainScreen].bounds.size.height == 568 )
			// iPhone 5
			image = [UIImage imageNamed:@"Default-568h"];
		else
			// iPhone 4
			image = [UIImage imageNamed:@"Default.png"];
	}
	
	
	// Instantiate image view and adjust rotation and origin
	UIImageView *splashView = [[UIImageView alloc] initWithImage:image];
	[self.window addSubview:splashView];
	splashView.transform = transform;
	CGRect frame = splashView.frame;
	frame.origin = origin;
	splashView.frame = frame;
	
	frame = CGRectInset( splashView.frame, -splashView.frame.size.width, -splashView.frame.size.height );
	[UIView animateWithDuration:0.5 delay:0 options:0 animations:^{
		splashView.alpha = 0;
		splashView.frame = frame;
	} completion:^(BOOL finished) {
		[splashView removeFromSuperview];
	}];
}

/* Returns the main view controller
 */
- (MainViewController*)mainViewController
{
	// iPhone or iPad?
	if( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
	{
		// iPad-specific interface here
		UISplitViewController *splitViewController = (UISplitViewController*)_window.rootViewController;
		return (splitViewController.viewControllers)[1];
	}
	else
	{
		// iPhone and iPod touch interface here
		return (MainViewController*)_window.rootViewController;
	}
}

- (void)applicationWillResignActive:(UIApplication *)application
{
	// Save preferred device
	[self savePreferredDevice];
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
	[self loadPreferredDevice];
}

- (void)savePreferredDevice
{
	DEBUG_POSITION;
	
	if( preferredDeviceUUID == nil )
		[[NSUserDefaults standardUserDefaults] removeObjectForKey:Constants.userDefaults_PreferredDeviceKey];
	else
		[[NSUserDefaults standardUserDefaults] setObject:[preferredDeviceUUID UUIDString] forKey:Constants.userDefaults_PreferredDeviceKey];

	[[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)loadPreferredDevice
{
	NSString *uuidString = [[NSUserDefaults standardUserDefaults] objectForKey:Constants.userDefaults_PreferredDeviceKey];
	if( [uuidString length] > 0 )
		preferredDeviceUUID = [[NSUUID alloc] initWithUUIDString:uuidString];
}

- (void)setAppearance
{
	[[UIToolbar appearance] setBackgroundImage:[UIImage imageNamed:@"toolbar.png"] forToolbarPosition:UIToolbarPositionAny barMetrics:UIBarMetricsDefault];
	[[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:@"toolbar.png"] forBarMetrics:UIBarMetricsDefault];

	NSShadow *shadow = [[NSShadow alloc] init];
	shadow.shadowOffset = CGSizeMake( 0, 1 );
	shadow.shadowColor = [UIColor blackColor];
	[[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor whiteColor],
														  NSShadowAttributeName: shadow }];
	[[UIBarButtonItem appearance] setTintColor:[UIColor whiteColor]];
}
@end
