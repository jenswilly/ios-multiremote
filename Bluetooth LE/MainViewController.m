//
//  ViewController.m
//  Bluetooth LE
//
//  Created by Jens Willy Johannsen on 30-04-12.
//  Copyright (c) 2012 Greener Pastures. All rights reserved.
//

//	Eek – old, crappy code in here... -JWJ

#import "MainViewController.h"
#import "CBUUID+Utils.h"
#import "MBProgressHUD.h"
#import "TBXML.h"
#import "PageListViewController.h"
#import "ButtonModel.h"
#import "Bluetooth_LE-Swift.h"

#define COMMAND_NUMBER @"commandNumber"

// Anonymous enum with tags
enum 
{
	TagConnectToDeviceActionSheet,
	TagJumpToPageActionSheet
} Tags;

// Coordinates for buttons
//static const CGFloat xCoords[] = {24, 117, 210};
//static const CGFloat yCoords[] = {7, 101, 195, 289};

@implementation MainViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

	// Set background for iPad
	// iPhone or iPad?
	if( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
		self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"metal_pattern.png"]];

	// Power up Bluetooth LE central manager (main queue)
	_centralManager = [[CBCentralManager alloc] initWithDelegate:self queue:nil options:@{ CBCentralManagerOptionRestoreIdentifierKey: @"MultiRemoteCentralManager"}];
	// Wait for callback to report BTLE ready.
	
	// Initialize
	_peripherals = [[NSMutableArray alloc] init];
	_peripheralNames = [[NSMutableDictionary alloc] init];
	
	// Load sound
	NSError *error = nil;
	audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"tock" ofType:@"aif"]] error:&error];
	NSAssert( error == nil, @"Error loading sound: %@", [error localizedDescription] );
    [audioPlayer prepareToPlay];
	
	// Set toolbar items
	_learnButton.enabled = NO;
	 
	// Load xml
	// iPhone or iPad?
	/// BUG: this shouldn't be necessary – maybe iOS 6 doesn't support ~iPad postfix for iPad files yet?
	if( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
		[self parseXMLFile:@"remote~iPad.xml"];
	else
		[self parseXMLFile:@"remote.xml"];

	// Power up Bluetooth LE central manager (main queue)
	//_centralManager = [[CBCentralManager alloc] initWithDelegate:self queue:nil options:@{ CBCentralManagerOptionRestoreIdentifierKey: @"MultiRemoteCentralManager"}];

	// Disable all buttons requiring a connection
	[self disableButtons];
	
	// Setup shadow properties for the toolbar
	_toolbar.layer.shadowColor = [UIColor redColor].CGColor;
	_toolbar.layer.shadowRadius = 40;
	_toolbar.layer.shadowOffset = CGSizeMake( 0, 22 );
	_toolbar.layer.shadowOpacity = 0.0f;
	
	commandMode = CommandModeIdle;
	
	// Listen to app did become active/resign active
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appDidBecomeActiveNotificationReceived:) name:UIApplicationDidBecomeActiveNotification object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillResignActiveNotificationReceived:) name:UIApplicationWillResignActiveNotification object:nil];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
	return UIStatusBarStyleLightContent;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
	// iPhone or iPad?
	if( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
		// All orientations supported for iPad
		return UIInterfaceOrientationMaskAll;
	else
		// iPhone supports only portrait orientations
		return UIInterfaceOrientationMaskPortrait;
}

- (void)appDidBecomeActiveNotificationReceived:(NSNotification*)notification
{
	if( _centralManager.state == CBManagerStatePoweredOn )
		[self attemptReconnect];
}

- (void)appWillResignActiveNotificationReceived:(NSNotification*)notification
{
	if( _connectedPeripheral != nil )
	{
		_isConnected = NO;
		[_centralManager cancelPeripheralConnection:_connectedPeripheral];
	}
}

#pragma mark - UIScrollViewDelegate methods

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
	// Calculate position for title scroller: one page in the main scroller corresponds to 80px in the title scroller
	CGFloat titleScrollerOffset = -([[UIScreen mainScreen] bounds].size.width/2) + _mainScroller.contentOffset.x / [[UIScreen mainScreen] bounds].size.width * 80;
	_pageLabelScroller.contentOffset = CGPointMake( titleScrollerOffset, 0 );
}

#pragma mark - Private methods

- (void)populateTitleLabelScroller:(NSArray*)pageTitles{
	CGFloat xPos = 0;
	CGFloat maxX = 0;
	
	for( NSString *pageTitle in pageTitles )
	{
		// Create label
		UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
		label.backgroundColor = [UIColor clearColor];
		label.font = [UIFont boldSystemFontOfSize:[UIFont systemFontSize]];
		label.textColor = [UIColor whiteColor];
		label.shadowColor = [UIColor blackColor];
		label.shadowOffset = CGSizeMake( 0, 1 );
		label.text = pageTitle;
		[label sizeToFit];
		
		// Position
		CGRect frame = label.frame;
		frame.origin.x = xPos - frame.size.width/2;
		frame.origin.y = _pageLabelScroller.bounds.size.height/2 - frame.size.height/2;
		label.frame = frame;
		xPos += 80;
		maxX = CGRectGetMaxX( label.frame );
		
		[_pageLabelScroller addSubview:label];
	}
	
	// Initial position
	_pageLabelScroller.contentSize = CGSizeMake( maxX, _pageLabelScroller.bounds.size.height );
	_pageLabelScroller.contentOffset = CGPointMake( -([[UIScreen mainScreen] bounds].size.width/2), 0 );
}

- (void)print:(NSString *)text
{
	NSMutableString *debugString = [text mutableCopy];
	if( ![NSThread currentThread].isMainThread )
		[debugString appendString:@" [THREAD!]"];
		 
	DEBUG_LOG( @"--> %@", debugString );
#if DEBUG
	_textView.text = [_textView.text stringByAppendingFormat:@"\r%@", debugString];
	[_textView scrollRangeToVisible:NSMakeRange( [_textView.text length]-1, 1 )];
#endif
}

- (void)scanTimeout:(NSTimer*)timer
{
	// Stop scanning
	[_centralManager stopScan];
	[MBProgressHUD hideHUDForView:self.view animated:YES];
	scanTimer = nil;

	[self print:@"Done scanning."];
	
	// We're done scanning for devices. If we're not yet connecting, let the user pick a device if we found any
	if( [_peripherals count] > 0 )
	{
		// Create action sheet style alert controller and add actions for each peripheral
		UIAlertController *actionController = [UIAlertController alertControllerWithTitle:@"Connect to device" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
		for( CBPeripheral *peripheral in _peripherals )
		{
			// Do we have a name for the device? Otherwise, use UUID
			NSString *peripheralDisplayName;
			if( _peripheralNames[ peripheral.identifier ] )
				peripheralDisplayName = _peripheralNames[ peripheral.identifier ];
			else
				peripheralDisplayName = [peripheral.identifier UUIDString];

			[actionController addAction:[UIAlertAction actionWithTitle:peripheralDisplayName style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
				[self connectToPeripheral:peripheral];
			}]];
		}

		// Add cancel button
		[actionController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];

		[self presentViewController:actionController animated:YES completion:nil];
	}
	else 
	{
		[self print:@"... no devices found."];

		UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"No devices found" message:@"Make sure the device is switched on and in range." preferredStyle:UIAlertControllerStyleAlert];
		UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
		[alert addAction:action];
		[self presentViewController:alert animated:YES completion:nil];

		// Set last item of the toolbar to the scan button (enabled)
		_learnButton.enabled = NO;
		_connectButton.enabled = YES;
		_connectButton.image = [UIImage imageNamed:@"barbtn_lightning"];
		
		// Remove info about preferred device (not really used since we always connect to the first one anyway).
		APP.preferredDeviceUUID = nil;
		[APP savePreferredDevice];
	}
}

- (void)sendCommand:(NSUInteger)commandNumber
{
	if( self.connectedPeripheral == nil )
	{
		[self print:@"Not connected."];
		return;
	}
	
	// Build command string
	NSString *commandPrefix = (learning ? @"L" : @"S");
	NSString *commandString = [NSString stringWithFormat:@"%@-%03d", commandPrefix, (int)commandNumber];
	[self print:[NSString stringWithFormat:@"Sending \"%@", commandString]];
	[_connectedPeripheral writeValue:[commandString dataUsingEncoding:NSUTF8StringEncoding] forCharacteristic:_GCACCommandCharacteristic type:CBCharacteristicWriteWithResponse];
	
	// Not learning anymore
	if( learning )
	{
		learning = NO;
		[self showLearningAnimation:learning];
	}
}

/**
 This method is called from the alert controller displaying the available peripherals.
 */
- (void)connectToPeripheral:(CBPeripheral*)peripheral
{
	DEBUG_LOG( @"Connecting to peripheral: %@", [peripheral description] );

	[MBProgressHUD showHUDAddedTo:self.view animated:YES];
	[_centralManager connectPeripheral:peripheral options:@{CBConnectPeripheralOptionNotifyOnDisconnectionKey: @YES}];
}


#pragma mark - CBCentralManagerDelegate methods

- (void)centralManagerDidUpdateState:(CBCentralManager *)central
{
	switch( central.state )
	{
        case CBManagerStatePoweredOn:
			[self print:@"Core Bluetooth ready."];
			if( ![self attemptReconnect] )
				[self scanAction:nil];
			break;
			
		default:
			[self print:[NSString stringWithFormat:@"CB State: %ld", (long)central.state]];
			break;
			
    }
}

#pragma mark → Peripheral discovered
- (void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI
{
	// If the timer has been killed, exit immediately. This is done so we don't get any "peripheral discovered" callbacks after the scan timer has been stopped.
	if( scanTimer == nil )
	{
		[self print:@"Peripheral discovered, but timer was stopped. Ignoring." ];
		return;
	}

	// Peripheral discovered: add it if we don't already have it
	BOOL alreadyFound = NO;
	for( CBPeripheral *p in _peripherals )
		if( [p.identifier isEqual:peripheral.identifier] )
			alreadyFound = YES;

	if( !alreadyFound )
		[_peripherals addObject:peripheral];

	// Add name if we have it. Otherwise, clear the entry so we don't have stale data.
	if( advertisementData[ CBAdvertisementDataLocalNameKey ] )
		_peripheralNames[ peripheral.identifier ] = advertisementData[ CBAdvertisementDataLocalNameKey ];
	else
		[_peripheralNames removeObjectForKey:peripheral.identifier];
	
	[self print:[NSString stringWithFormat:@"Found peripheral '%@'. Preferred: %d", [peripheral.identifier UUIDString], ([peripheral.identifier isEqual:APP.preferredDeviceUUID])]];
	
	// Connect to first found device
	[self print:@"Found device – connecting..." ];
	[_centralManager stopScan];
	[scanTimer invalidate];	// So we don't get to the "done scanning" method
	scanTimer = nil;
	
	// Connect...
	[_centralManager connectPeripheral:peripheral options:@{ CBConnectPeripheralOptionNotifyOnDisconnectionKey: @YES }];
}

- (void)centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral
{
	// Hide HUD
	[MBProgressHUD hideAllHUDsForView:self.view animated:YES];

	[self print:[NSString stringWithFormat:@"Connected to peripheral: %@", [peripheral.identifier UUIDString]]];
	
	// Set property and remember as preferred device
	peripheral.delegate = self;
	self.connectedPeripheral = peripheral; 
	APP.preferredDeviceUUID = peripheral.identifier;

	// Save to shared defaults
	NSUserDefaults *sharedDefaults = [[NSUserDefaults alloc] initWithSuiteName:@"group.dk.greenerpastures.bluetoothRemote"];
	[sharedDefaults setObject:peripheral.identifier.UUIDString forKey:@"kDefaultsKey_PreferredDeviceUUID"];
	[sharedDefaults synchronize];
	
	// Find the GCAC service
	[peripheral discoverServices:@[ [CBUUID UUIDWithString:Constants.GCACServiceUUID] ]];
}

- (void)centralManager:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error
{
	// Check for error
	if( error != nil )
	{
		// Error disconnecting.
		[self print:[NSString stringWithFormat:@"Error disconnecting peripheral: %@", [error localizedDescription]]];
	}

	[self print:@"Peripheral disconnected"];
	self.connectedPeripheral = nil;
	
	// Disable all buttons requiring a connection
	[self disableButtons];
	_learnButton.enabled = NO;
	_connectButton.image = [UIImage imageNamed:@"barbtn_lightning"];

	// Was this an intentional disconnect?
	if( _isConnected )
	{
		// No, we should still be connected. Try to re-connect
		[self print:@"Attempting re-connect"];
		[self attemptReconnect];
//		[self scanAction:nil];
	}
}

- (void)centralManager:(CBCentralManager *)central didFailToConnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error
{
	[self print:[NSString stringWithFormat:@"Could not connect to peripheral. Error: %@", [error localizedDescription] ]];
}

- (void)centralManager:(CBCentralManager *)central didRetrieveConnectedPeripherals:(NSArray *)connectedPeripherals
{
	DEBUG_LOG( @"Connected peripherals: %@", connectedPeripherals );
}

- (void)centralManager:(CBCentralManager *)central didRetrievePeripherals:(NSArray *)foundPeripherals
{
	DEBUG_LOG( @"Found peripherals: %@", foundPeripherals );
}

- (void)centralManager:(CBCentralManager *)central willRestoreState:(NSDictionary *)dict
{
	[self print:[NSString stringWithFormat:@"willRestoreState: %@", dict]];

	// Get connected peripherals
	NSArray *peripherals = dict[ CBCentralManagerRestoredStatePeripheralsKey ];

	// Add to array and set delegate to self
	for( CBPeripheral *peripheral in peripherals )
	{
		[_peripherals addObject:peripheral];
		peripheral.delegate = self;
	}


}

#pragma mark - CBPeripheralDelegate methods

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error
{
	// Check for error
	if( error != nil )
	{
		// Error
		[self print:[NSString stringWithFormat:@"Error discovering services: %@", [error localizedDescription]]];
		return;
	}
	
	[self print:@"Done discovering services."];
	
	// Find the GCAC service (altough it's the only one we're trying to discover)
	for( CBService *service in peripheral.services )
	{
		if( [service.UUID isEqualToUUIDString:Constants.GCACServiceUUID] )
		{
			self.GCACService = service;
			[self print:@"Found GCAC service"];
			
			// Find characteristics from GCAC service
			[_connectedPeripheral discoverCharacteristics:@[[CBUUID UUIDWithString:Constants.GCACCommandCharacteristic]] forService:service];
		}
	}
}

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error
{
	// Check for error
	if( error != nil )
	{
		// Error
		[self print:[NSString stringWithFormat:@"Error discovering characteristics: %@", [error localizedDescription]]];
		return;
	}
	
	// Which service did we find characteristics for?
	if( service == _GCACService )
	{
		// The GCAC service: get pointers to command and response characteristics
		for( CBCharacteristic *characteristic in service.characteristics )
		{
			if( [characteristic.UUID isEqualToUUIDString:Constants.GCACCommandCharacteristic] )
			{
				[self print:@"Found command characteristic"];
				self.GCACCommandCharacteristic = characteristic;
				
				// We have the command characteristic: enable buttons
				[self enableButtons];
				
				// Set toolbar items
				_learnButton.enabled = YES;
				_connectButton.enabled = YES;
				_connectButton.image = [UIImage imageNamed:@"barbtn_disconnect"];

				// We are now properly connected
				_isConnected = YES;
				[self print:@"isConnected = YES"];
			}
			/* -- We don't care about the response characteristic right now
			else if( [characteristic.UUID isEqualToUUIDString:UUID_RESPONSE_CHARACTERISTIC] )
			{
				[self print:@"Found response characteristic"];
				self.GCACResponseCharacteristic = characteristic;
				
				// Enable notifications for response characteristic
				[peripheral setNotifyValue:YES forCharacteristic:characteristic];
			}
			 */
		}
	}
}

- (void)peripheral:(CBPeripheral *)peripheral didUpdateNotificationStateForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
	// Check for error
	if( error != nil )
	{
		// Error
		[self print:@"Error enabling notifications for response characteristic"];
		return;
	}
	
	[self print:@"Response notifications enabled"];
}

- (void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
	// Check for error
	if( error != nil )
	{
		// Error
		[self print:[NSString stringWithFormat:@"Error updating value for characteristic %@: %@", [characteristic.UUID string], [error localizedDescription]]];
		return;
	}
	
	// Get user friendly name for characteristic if possible
	NSString *characteristicName;
	if( characteristic == _GCACCommandCharacteristic )
		characteristicName = @"COMMAND";
	else if( characteristic == _GCACResponseCharacteristic )
		characteristicName = @"RESPONSE";
	else 
		characteristicName = [characteristic.UUID string];
	
	NSString *valueString = [[NSString alloc] initWithData:characteristic.value encoding:NSUTF8StringEncoding];
	[self print:[NSString stringWithFormat:@"Value updated for %@ = '%@'", characteristicName, valueString]];
}

- (void)peripheral:(CBPeripheral *)peripheral didWriteValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
	// Check for error
	if( error != nil )
	{
		// Error
		[self print:[NSString stringWithFormat:@"Error writing command value: %@", [error localizedDescription]]];
		return;
	}
	
	// Should we continue to send the command?
	if( commandMode == CommandModeRepeat )
		// Yes
		[self sendCommand:currentCommandNumber];
}

#pragma mark - UISplitViewController delegate methods

/*
- (void)splitViewController:(UISplitViewController *)svc willHideViewController:(UIViewController *)aViewController withBarButtonItem:(UIBarButtonItem *)barButtonItem forPopoverController:(UIPopoverController *)pc
{
	[barButtonItem setImage:[UIImage imageNamed:@"barbtn_debug"]];

	// Add the button to toolbar items array
	NSArray *items = @[barButtonItem, _flexSpace, _learnButton, _connectButton, _debugButton];
	[_toolbar setItems:items animated:YES];
	
	// Remember popover
	self.popover = pc;
}
 */

- (void)splitViewController:(UISplitViewController *)svc willChangeToDisplayMode:(UISplitViewControllerDisplayMode)displayMode
{
	// Old - (void)splitViewController:(UISplitViewController *)svc willShowViewController:(UIViewController *)vc invalidatingBarButtonItem:(UIBarButtonItem *)item
	if( displayMode == UISplitViewControllerDisplayModeAllVisible )
	{
		// Remove the bar button from toolbar items
		NSArray *items = @[_flexSpace, _learnButton, _connectButton, _debugButton];
		[_toolbar setItems:items animated:YES];
	}
}

#pragma mark - ButtonModelDelegate methods

- (void)repeatCommand:(UIButton*)sender
{
	currentCommandNumber = sender.tag;
	commandMode = CommandModeRepeat;
	
	// Play sound
	[audioPlayer play];
	
	// Send command
	[self sendCommand:currentCommandNumber];
}

/* Stops the repeatable command from transmitting.
 */
- (void)cancelRepeatCommand:(id)sender
{
	// Stop repeating
	commandMode = CommandModeIdle;
}

- (void)sendCommandAction:(UIButton*)sender
{
	// Play sound
	[audioPlayer play];
	
	[self sendCommand:sender.tag];
}

#pragma mark - XML parser methods

- (void)parseXMLFile:(NSString*)xmlFileName
{
	/// TEMP: get from iTunes documents instead
	NSError *error = nil;
	TBXML *xml = [TBXML newTBXMLWithXMLFile:xmlFileName error:&error];
	NSAssert( error == nil, @"Error opening xml file: %@", [error localizedDescription] );
	
	// Iterate pages
	// iPhone or iPad?
	if( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
		// iPad: parse and create 
		[self iPad_readPages:xml.rootXMLElement];
	else
		// iPhone: parse and add to mainScroller and titleScroller
		[self iPhone_readPages:xml.rootXMLElement];
}

- (void)iPad_readPages:(TBXMLElement*)rootElement
{
	// Get layout info
	NSString *layoutPlistFilePath;
	
	// iPhone or iPad?
	/// BUG: shouldn't be necessary – iOS 6 doesn't support ~iPad postfix
	if( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
		layoutPlistFilePath = [[NSBundle mainBundle] pathForResource:@"coordinates~iPad" ofType:@"plist"];
	else
		layoutPlistFilePath = [[NSBundle mainBundle] pathForResource:@"coordinates" ofType:@"plist"];

	NSDictionary *layoutInfo = [NSDictionary dictionaryWithContentsOfFile:layoutPlistFilePath];
	NSAssert( layoutInfo != nil, @"Error opening layout coordinates file!" );
	
	// Temporary array and dictionary for holding page names and buttons
	NSMutableArray *tmpArray = [[NSMutableArray alloc] init];
	NSMutableDictionary *tmpDictionary = [[NSMutableDictionary alloc] init];
	
	// Iterate pages
	TBXMLElement *page = [TBXML childElementNamed:@"page" parentElement:rootElement];
	while( page )
	{
		int x=0, y=0;	// Coordinates
		
		// Add page name to array
		NSString *pageName = [TBXML valueOfAttributeNamed:@"name" forElement:page];
		[tmpArray addObject:pageName];
		DEBUG_LOG( @"Found page '%@'", pageName );
		
		// Which layout?
		NSString *layout = [TBXML valueOfAttributeNamed:@"layout" forElement:page];
		NSDictionary *pageLayoutInfo = layoutInfo[layout];
		NSAssert1( pageLayoutInfo, @"Unknown page layout: %@", layout );
		
		// Get coords from plist dictionary
		int columns = [pageLayoutInfo[@"columns"] intValue];
		int rows = [pageLayoutInfo[@"rows"] intValue];
		NSString *filenameFormat = pageLayoutInfo[@"filenameFormat"];
		CGFloat initialX = [pageLayoutInfo[@"initialX"] floatValue];
		CGFloat initialY = [pageLayoutInfo[@"initialY"] floatValue];
		CGFloat stepX = [pageLayoutInfo[@"stepX"] floatValue];
		CGFloat stepY = [pageLayoutInfo[@"stepY"] floatValue];
		
		// Create view for this page
		UIView *pageView = [[UIView alloc] initWithFrame:_contentView.bounds];
		pageView.backgroundColor = [UIColor clearColor];
		
		// Iterate rows
		CGFloat xPos;
		CGFloat yPos = initialY;	// Initial y position
		TBXMLElement *rowElement = [TBXML childElementNamed:@"row" parentElement:page];
		while( rowElement )
		{
			// Reset x position
			xPos = initialX;
			
			// Iterate buttons
			TBXMLElement *buttonElement = [TBXML childElementNamed:@"button" parentElement:rowElement];
			while( buttonElement )
			{
				// Instantiate button from XML
				ButtonModel *buttonModel = [ButtonModel buttonModelFromXMLNode:buttonElement];
				
				// Create and configure button
				UIButton *btn = [buttonModel buttonForDelegate:self filenameFormat:filenameFormat];
				if( btn )
				{
					CGRect frame = btn.frame;
					frame.origin = CGPointMake( xPos, yPos );
					btn.frame = frame;
					
					// Add to view
					[pageView addSubview:btn];
				}
				
				// Next button
				xPos += stepX;
				NSAssert( x < columns, @"Too many buttons in row: %s", rowElement->text );
				x++;
				buttonElement = [TBXML nextSiblingNamed:@"button" searchFromElement:buttonElement];
			}
			
			// Next row
			yPos += stepY;
			x = 0;
			NSAssert( y < rows, @"Too many rows in page: %s", page->text );
			y++;
			rowElement = [TBXML nextSiblingNamed:@"row" searchFromElement:rowElement];
		}
		
		// Store view in dictionary with page name as key
		tmpDictionary[pageName] = pageView;
		
		// Next page
		page = [TBXML nextSiblingNamed:@"page" searchFromElement:page];
	}
	
	// Set array and dictionary
	_pages = [[NSArray alloc] initWithArray:tmpArray];
	_pageContent = [[NSDictionary alloc] initWithDictionary:tmpDictionary];
	
	/// TEMP: select first page
	self.currentButtonsView = _pageContent[ _pages[ 0 ]];
	_currentPageName = _pages[ 0 ];

	// Reload pages list
	[_masterViewController.tableView reloadData];
}

- (void)iPhone_readPages:(TBXMLElement*)rootElement
{
	// Get iPhone layout info
	NSString *layoutPlistFilePath = [[NSBundle mainBundle] pathForResource:@"coordinates" ofType:@"plist"];

	// Override for iPhone X
	if( [[UIScreen mainScreen] scale] == 3 && [UIScreen mainScreen].bounds.size.width == 375 )
		layoutPlistFilePath = [[NSBundle mainBundle] pathForResource:@"coordinates_X" ofType:@"plist"];

	NSDictionary *layoutInfo = [NSDictionary dictionaryWithContentsOfFile:layoutPlistFilePath];
	NSAssert( layoutInfo != nil, @"Error opening layout coordinates file!" );
	
	TBXMLElement *page = [TBXML childElementNamed:@"page" parentElement:rootElement];
	NSMutableArray *pageNames = [NSMutableArray array];
	CGFloat pageOffset = 0;
	while( page )
	{
		int x=0, y=0;	// Coordinates
		NSString *pageName = [TBXML valueOfAttributeNamed:@"name" forElement:page];
		[pageNames addObject:pageName];
		DEBUG_LOG( @"Found page '%@'", pageName );
		
		// Which layout?
		NSString *layout = [TBXML valueOfAttributeNamed:@"layout" forElement:page];
		NSDictionary *pageLayoutInfo = layoutInfo[layout];
		NSAssert1( pageLayoutInfo, @"Unknown page layout: %@", layout );
		
		// Get coords from plist dictionary
		int columns = [pageLayoutInfo[@"columns"] intValue];
		int rows = [pageLayoutInfo[@"rows"] intValue];
		NSString *filenameFormat = pageLayoutInfo[@"filenameFormat"];
		CGFloat buttonHeight = [pageLayoutInfo[ @"buttonHeight" ] floatValue];
		CGFloat stepX = [pageLayoutInfo[@"stepX"] floatValue];
		CGFloat stepY = [pageLayoutInfo[@"stepY"] floatValue];
		CGFloat initialX = [pageLayoutInfo[@"initialX"] floatValue];
		CGFloat initialY;
		if( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
			initialY = [pageLayoutInfo[@"initialY"] floatValue];	// iPad version still uses initialY from layout XML file
		else
			// iPhone
			initialY = ([UIScreen mainScreen].bounds.size.height - (rows-1) * stepY - buttonHeight)/2 - _mainScroller.frame.origin.y;	// `stepY` is the height of both button _and_ spacer. So only rows-1 times step + one extra button height without spacer

		// Iterate rows
		CGFloat xPos;
		CGFloat yPos = initialY;	// Initial y position
		TBXMLElement *rowElement = [TBXML childElementNamed:@"row" parentElement:page];
		while( rowElement )
		{
			// Reset x position
			xPos = initialX;
			
			// Iterate buttons
			TBXMLElement *buttonElement = [TBXML childElementNamed:@"button" parentElement:rowElement];
			while( buttonElement )
			{
				// Instantiate button from XML
				ButtonModel *buttonModel = [ButtonModel buttonModelFromXMLNode:buttonElement];
				
				// Create and configure button
				UIButton *btn = [buttonModel buttonForDelegate:self filenameFormat:filenameFormat];
				CGRect frame = btn.frame;
				frame.origin = CGPointMake( xPos + pageOffset, yPos );
				btn.frame = frame;
				
				// Add it
				[_mainScroller addSubview:btn];
				
				// Next button
				xPos += stepX;
				NSAssert( x < columns, @"Too many buttons in row: %s", rowElement->text );
				x++;
				buttonElement = [TBXML nextSiblingNamed:@"button" searchFromElement:buttonElement];
			}
			
			// Next row
			yPos += stepY;
			x = 0;
			NSAssert( y < rows, @"Too many rows in page: %s", page->text );
			y++;
			rowElement = [TBXML nextSiblingNamed:@"row" searchFromElement:rowElement];
		}
		
		// Next page
		pageOffset += _mainScroller.bounds.size.width;
		page = [TBXML nextSiblingNamed:@"page" searchFromElement:page];
	}
	
	// Set content size
	_mainScroller.contentSize = CGSizeMake( pageOffset, _mainScroller.bounds.size.height );
	
	// Set page titles
	[self populateTitleLabelScroller:pageNames];
}

#pragma mark - Other methods

- (void)showLearningAnimation:(BOOL)isLearning
{
	// Create fade-up/fade-down animation to be shown on toolbar and buttons
	CABasicAnimation *anim = [CABasicAnimation animationWithKeyPath:@"shadowOpacity"];
    anim.fromValue = @0.0f;
    anim.toValue = @0.85f;
    anim.duration = 1.0;
	anim.autoreverses = YES;
	anim.repeatCount = MAXFLOAT;	// I.e. infinite
	
	// Are we learnign?
	if( isLearning )
		// Yes: add shadow animation
		[_toolbar.layer addAnimation:anim forKey:@"shadowOpacity"];
	else 
	{
		// No: remove animation
		[_toolbar.layer removeAllAnimations];
		_toolbar.layer.shadowOpacity = 0.0f;
	}
	
	// Change font color on all buttons
	// iPhone or iPad?
	if( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
	{
		// iPad: all pages
		for( UIView *view in [_pageContent allValues] )
		{
			// Individual buttons
			for( UIView *subview in view.subviews )
			{
				if( [subview isKindOfClass:[UIButton class]] )
				{
					UIButton *button = (UIButton*)subview;	// Typecast
					if( isLearning )
					{
						[button setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
						[button setTitleShadowColor:[UIColor whiteColor] forState:UIControlStateNormal];
						
						// Start shadow animation
						[button.layer addAnimation:anim forKey:@"shadowOpacity"];
					}
					else 
					{
						[button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
						[button setTitleShadowColor:[UIColor blackColor] forState:UIControlStateNormal];
						
						// Stop animation
						[button.layer removeAllAnimations];
						button.layer.shadowOpacity = 0.0f;
					}
				}
			}	// end-for: subviews
		}	// end-for: [pageContent allValues]
	}
	else
	{
		// iPhone: use mainScroller
		for( UIView *subview in _mainScroller.subviews )
		{		
			if( [subview isKindOfClass:[UIButton class]] )
			{
				UIButton *button = (UIButton*)subview;	// Typecast for your convenience
				if( isLearning )
				{
					[button setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
					[button setTitleShadowColor:[UIColor whiteColor] forState:UIControlStateNormal];
					
					// Show shadow. Performance is not good enough for animation
					button.layer.shadowOpacity = 0.85f;
				}
				else 
				{
					[button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
					[button setTitleShadowColor:[UIColor blackColor] forState:UIControlStateNormal];
					
					// Hide shadow
					button.layer.shadowOpacity = 0.0f;
				}
			}
		}	// end-for: _mainScroller.subviews
	}
}

- (void)showSplash
{
	UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
	DEBUG_LOG( @"showSplash: orientation: %d", (int)orientation );
	
	// Create image view with image that match device and orientation
	UIImage *image;
	CGAffineTransform transform = CGAffineTransformIdentity;
	CGPoint origin = CGPointZero;
	
	if( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
	{
		// iPad: which orientation?
//		[[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
//		UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
		if( UIInterfaceOrientationIsLandscape( orientation ))
		{
			// We need to rotate image view
			if( orientation == UIDeviceOrientationLandscapeLeft )
			{
				transform = CGAffineTransformMakeRotation( M_PI_2 );
				//	origin = CGPointMake( -20, 0 );
			}
			else if( orientation == UIDeviceOrientationLandscapeRight )
			{
				transform = CGAffineTransformMakeRotation( -M_PI_2 );
				origin = CGPointMake( 20, 0 );
			}
			
			// Load image
			image = [UIImage imageNamed:@"Default-Landscape.png"];
			
		}
		else
		{
			// Rotate if upside-down
			if( orientation == UIDeviceOrientationPortraitUpsideDown )
				transform = CGAffineTransformMakeRotation( M_PI );
			else
				origin = CGPointMake( 0, 20 );
			
			image = [UIImage imageNamed:@"Default-Portrait.png"];
		}
	}
	else
	{
		// iPhone
		image = [UIImage imageNamed:@"Default.png"];
		origin = CGPointMake( 0, -20 );		// Adjust for status bar
	}
	
	
	// Instantiate image view and adjust rotation and origin
	UIImageView *splashView = [[UIImageView alloc] initWithImage:image];
	DEBUG_LOG( @"splashView: %@", splashView );
	[APP.window addSubview:splashView];
	splashView.transform = transform;
	CGRect frame = splashView.frame;
	frame.origin = origin;
	splashView.frame = frame;
	
	frame = CGRectInset( splashView.frame, -splashView.frame.size.width, -splashView.frame.size.height );
//	[UIView animateWithDuration:0.5 delay:1 options:0 animations:^{
//		splashView.alpha = 0;
//		splashView.frame = frame;
//	} completion:^(BOOL finished) {
//		[splashView removeFromSuperview];
//	}];
}

- (void)disableButtons
{
	// iPhone or iPad?
	if( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
	{
		// iPad: disable buttons in all views
		for( UIView *view in [_pageContent allValues] )
		{
			for( UIView *subview in view.subviews )
				if( [subview isKindOfClass:[UIButton class]] )
					[(UIButton*)subview setEnabled:NO];
		}
	}
	else
	{
		// iPhone: enumerate subviews in mainscroller
		for( UIView *subview in _mainScroller.subviews )
			if( [subview isKindOfClass:[UIButton class]] )
				[(UIButton*)subview setEnabled:NO];
	}
}

- (void)enableButtons
{
	// iPhone or iPad?
	if( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
	{
		// iPad: enable buttons in all views
		for( UIView *view in [_pageContent allValues] )
		{
			for( UIView *subview in view.subviews )
				if( [subview isKindOfClass:[UIButton class]] )
					[(UIButton*)subview setEnabled:YES];
		}
	}
	else
	{
		// iPhone: enumerate subviews of mainscroller and enable all buttons
		for( UIView *subview in _mainScroller.subviews )
			if( [subview isKindOfClass:[UIButton class]] )
				[(UIButton*)subview setEnabled:YES];
	}
}

- (void)setCurrentButtonsView:(UIView*)currentButtonsView
{
	// Do nothing if we're already showing this view
	if( _currentButtonsView == currentButtonsView )
		return;
	
	// Remember the current view so we can animate it
	UIView *oldButtonsView = _currentButtonsView;
	
	// Update ivar
	_currentButtonsView = currentButtonsView;
	
	// Add and fade in new buttons view
	CGAffineTransform transform = CGAffineTransformMakeScale( 0.1, 0.1 );
	_currentButtonsView.alpha = 0;
	_currentButtonsView.transform = transform;
	[self.contentView addSubview:_currentButtonsView];
	[UIView animateWithDuration:0.3f animations:^{
		self->_currentButtonsView.alpha = 1;
		self->_currentButtonsView.transform = CGAffineTransformIdentity;
		
		oldButtonsView.transform = transform;
		oldButtonsView.alpha = 0;
	} completion:^(BOOL finished) {
		[oldButtonsView removeFromSuperview];
	}];
	
	// Dismiss popover controller if visible
//	[_popover dismissPopoverAnimated:YES];
}

- (BOOL)attemptReconnect
{
	// Only scan if BT is powered up
	if( _centralManager.state != CBManagerStatePoweredOn ) {
		[self print:@"Bluetooth off – not reconnecting."];
		return NO;
	}

	// Attempt reconnect
	if( APP.preferredDeviceUUID != nil )
	{
		NSArray<CBPeripheral*> *peripherals = [_centralManager retrievePeripheralsWithIdentifiers:@[ APP.preferredDeviceUUID ]];
		if( peripherals.firstObject != nil ) {
			[self print: @"Connecting to connected device"];
			self.connectedPeripheral = peripherals.firstObject;
			[_centralManager connectPeripheral:peripherals.firstObject options:nil];
			return YES;
		}
	}
	
	// Else attempt to find any device with the GCAC service
	CBUUID *serviceUUID = [CBUUID UUIDWithString:Constants.GCACServiceUUID];
	NSArray<CBPeripheral*> *servicePeripherals = [_centralManager retrieveConnectedPeripheralsWithServices:@[ serviceUUID ]];
	if( servicePeripherals.firstObject != nil ) {
		[self print: @"Connecting to service device"];
		self.connectedPeripheral = servicePeripherals.firstObject;
		[_centralManager connectPeripheral:servicePeripherals.firstObject options:nil];
		return YES;
	}

	return NO;
}

- (IBAction)scanAction:(id)sender
{
	// Only scan if BT is powered up
	if( _centralManager.state != CBManagerStatePoweredOn )
	{
		[self print:@"Bluetooth off – not scanning."];
		return;
	}
	
	// Are we are currently connected?
	if( self.connectedPeripheral )
	{
		// Yes: disconnect instead
		[self print:@"Disconnecting from scanAction..."];
		[self disconnectAction:nil];
		return;
	}
	
	// Show HUD
	[MBProgressHUD showHUDAddedTo:self.view animated:YES];
	
	// We're not yet connected
	_isConnected = NO;
	[self print:@"isConnected = NO"];

	// Start scanning - we're only looking for devices with the "Generic Command and Control Protocol" service
	[self print:[NSString stringWithFormat:@"Start scanning. Current preferred peripheral is '%@'.", [APP.preferredDeviceUUID UUIDString]]];
	scanTimer = [NSTimer timerWithTimeInterval:Constants.scanTimeout target:self selector:@selector(scanTimeout:) userInfo:nil repeats:NO];
	[[NSRunLoop mainRunLoop] addTimer:scanTimer forMode:NSRunLoopCommonModes];
	
	[_centralManager scanForPeripheralsWithServices:@[ [CBUUID UUIDWithString:Constants.GCACServiceUUID] ] options:nil];
	
	// Set toolbar items
	_learnButton.enabled = NO;
	_connectButton.enabled = NO;
}

- (IBAction)disconnectAction:(id)sender
{
	if( self.connectedPeripheral == nil )
	{
		[self print:@"Not connected."];
		return;
	}

	[self print:@"Disconnecting..."];
	_isConnected = NO;
	[self print:@"isConnected = NO"];
	[_centralManager cancelPeripheralConnection:_connectedPeripheral];
	self.connectedPeripheral = nil;
}

- (IBAction)clearAction:(id)sender
{
	_textView.text = @"";
}

- (IBAction)forgetPreferredAction:(id)sender
{
	// Forget preferred device
	APP.preferredDeviceUUID = nil;
	[APP savePreferredDevice];
}

- (IBAction)readAction:(id)sender
{
	if( self.connectedPeripheral == nil )
	{
		[self print:@"Not connected."];
		return;
	}
	
	[self print:@"Reading response characteristic..."];
	[_connectedPeripheral readValueForCharacteristic:_GCACResponseCharacteristic];
}

- (IBAction)learn:(id)sender
{
	// Toggle recording
	learning = !learning;
	[self showLearningAnimation:learning];
}

- (IBAction)toggleDebugAction:(id)sender
{
	// Is the debugging view already visible?
	if( _debugView.frame.origin.y < self.view.bounds.size.height )
	{
		// Yes: hide it
		CGRect frame = _debugView.frame;
		frame.origin.y = self.view.bounds.size.height;
		
		[UIView animateWithDuration:0.3 animations:^{
			self->_debugView.frame = frame;
		}];
	}
	else
	{
		// No: show it
		CGRect frame = _debugView.frame;
		frame.origin.y = self.view.bounds.size.height - frame.size.height;

		// Scroll to bottom
		[_textView scrollRangeToVisible:NSMakeRange( [_textView.text length]-1, 1 )];

		[UIView animateWithDuration:0.3 animations:^{
			self->_debugView.frame = frame;
		}];
	}
}

- (IBAction)debug1Action:(id)sender
{
	BOOL isEnabled = NO;
	
	// Are the buttons currently enabled?
	// iPhone or iPad?
	if( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
	{
		// iPad: get first button from currentButtonsView
		for( UIView *subview in _currentButtonsView.subviews )
		{
			if( [subview isKindOfClass:[UIButton class]] )
			{
				isEnabled = [(UIButton*)subview isEnabled];
				break;
			}
		}
	}
	else
	{
		// iPhone: get first button in mainScroller
		for( UIView *subview in _mainScroller.subviews )
		{
			if( [subview isKindOfClass:[UIButton class]] )
			{
				isEnabled = [(UIButton*)subview isEnabled];
				break;
			}
		}
	}

	// Enable or disable all buttons
	if( isEnabled )
	{
		[self disableButtons];
		
		// Set toolbar items
		_learnButton.enabled = NO;
		_connectButton.enabled = YES;
		_connectButton.image = [UIImage imageNamed:@"barbtn_lightning"];
		self.connectedPeripheral = nil;
	}
	else 
	{
		[self enableButtons];
		
		// Set toolbar items
		_learnButton.enabled = YES;
		_connectButton.enabled = YES;
		_connectButton.image = [UIImage imageNamed:@"barbtn_disconnect"];
	}
}

- (IBAction)choosePageAction:(id)sender
{
	UIAlertController *actionController = [UIAlertController alertControllerWithTitle:@"Jump to page" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
	__block NSUInteger i = 0;
	for( UILabel *subview in _pageLabelScroller.subviews )
	{
		// Make sure it's a label
		if( ![subview isKindOfClass:[UILabel class]] )
			continue;

		[actionController addAction:[UIAlertAction actionWithTitle:subview.text style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
			[self->_mainScroller setContentOffset:CGPointMake( self->_mainScroller.bounds.size.width * i++, 0) animated:YES];
		}]];
	}

	[actionController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
	[self presentViewController:actionController animated:YES completion:nil];
}

- (IBAction)sendLearnAction:(id)sender
{
	if( self.connectedPeripheral == nil )
	{
		[self print:@"Not connected."];
		return;
	}
	
	[self print:@"Sending \"L-000\""];
	[_connectedPeripheral writeValue:[@"L-000" dataUsingEncoding:NSUTF8StringEncoding] forCharacteristic:_GCACCommandCharacteristic type:CBCharacteristicWriteWithResponse];
}

- (IBAction)sendTAction:(id)sender
{
	if( self.connectedPeripheral == nil )
	{
		[self print:@"Not connected."];
		return;
	}
	
	[self print:@"Sending \"T-000\""];
	[_connectedPeripheral writeValue:[@"T-000" dataUsingEncoding:NSUTF8StringEncoding] forCharacteristic:_GCACCommandCharacteristic type:CBCharacteristicWriteWithResponse];
}

- (IBAction)sendYAction:(id)sender
{
	if( self.connectedPeripheral == nil )
	{
		[self print:@"Not connected."];
		return;
	}
	
	[self print:@"Sending \"Y-000\""];
	[_connectedPeripheral writeValue:[@"Y-000" dataUsingEncoding:NSUTF8StringEncoding] forCharacteristic:_GCACCommandCharacteristic type:CBCharacteristicWriteWithResponse];
}

@end
