//
//  Bluetooth LE
//
//  Created by Jens Willy Johannsen on 25/01/16.
//  Copyright © 2016 Greener Pastures. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UISplitViewController (StatusBar)

- (UIStatusBarStyle)preferredStatusBarStyle;

@end
