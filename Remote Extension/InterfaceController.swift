//
//  InterfaceController.swift
//  Remote Extension
//
//  Created by Jens Willy Johannsen on 21/09/2017.
//  Copyright © 2017 Greener Pastures. All rights reserved.
//

import WatchKit
import Foundation
import CoreBluetooth

class InterfaceController: WKInterfaceController, CBCentralManagerDelegate, CBPeripheralDelegate, WKCrownDelegate
{
	@IBOutlet var testButton: WKInterfaceButton!
	@IBOutlet var buttonGroup: WKInterfaceGroup!
	@IBOutlet var buttonImage: WKInterfaceImage!
	
	private var _centralManager: CBCentralManager? = nil
	private var _preferredDeviceUUIDString: String?
	private var _scanTimeoutTimer: Timer?
	private var _peripheral: CBPeripheral?
	private var _service: CBService?
	private var _commandCharacteristic: CBCharacteristic?
	private var _sharedDefaults: UserDefaults?
	private var _connectionTimer: Timer?
	private var _rotation: Double = 0	// Accumulated rotation

	enum VolumeDirection
	{
		case up
		case down
	}
	
	override func willActivate()
	{
		NSLog( "Will activate" )
		
		if _peripheral == nil {
			// Not connected
			buttonGroup.setBackgroundColor( #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1) )
		} else {
			buttonGroup.setBackgroundColor( #colorLiteral(red: 0.721568644, green: 0.8862745166, blue: 0.5921568871, alpha: 1) )
		}
		
		_sharedDefaults = UserDefaults( suiteName: Config.sharedDefaultsSuiteName )
		_preferredDeviceUUIDString = _sharedDefaults?.object( forKey: Config.defaultsKey_PreferredDeviceUUID ) as! String?

		if _centralManager == nil {
			_centralManager = CBCentralManager( delegate: self, queue: nil, options: [CBCentralManagerOptionRestoreIdentifierKey: Config.coreBluetoothRestoreIdentifier] )
		} else {
			attemptReconnect()
		}
		
		// Get crown callbacks
		self.crownSequencer.delegate = self
		self.crownSequencer.focus()
	}
	
	override func didDeactivate()
	{
		NSLog( "Did deactivate" )

		// Disconnect if connected
		_centralManager?.stopScan()
		_scanTimeoutTimer = nil
		
		if let peripheral = _peripheral, let centralManager = _centralManager {
			NSLog( "Disconnecting from \(peripheral) in extension viewWillDisappear()" )
			centralManager.cancelPeripheralConnection( peripheral )
			_peripheral = nil
		}
}
	
 	override func didAppear()
	{
		super.didAppear()
		NSLog( "Did appear" )
	}
	
	override func willDisappear()
	{
		super.willDisappear()
		NSLog( "will disappear" )
	}

	// MARK: - Actions
	
	@IBAction func testBtnAction()
	{
		if _peripheral == nil {
			startScan()
		} else {
			sendCommand( 8 )	// 8 = mute
		}
	}
	
	@IBAction func powerOffAction()
	{
		guard _peripheral != nil else {
			// Not connected
			return
		}

		sendCommand( 3 )	// 3 = power
	}

	// MARK: - Timers
	
	@objc func scanTimeout()
	{
		NSLog( "Scan timeout. State currently: \(String(describing: _centralManager?.state))" )
		_centralManager?.stopScan()
		_scanTimeoutTimer = nil
		
		// Reset button image
		stopScan()
		
		let cancelAction = WKAlertAction(title: "OK", style: .default) {}
		presentAlert(withTitle: "Scan done", message: "Found \(String( describing: _peripheral ))", preferredStyle: .alert, actions: [cancelAction] )
		
		if _peripheral == nil {
			// Didn't find any
			disableButtons()
		}
	}
	
	/// Timeout when connecting. No connection has been established (and no error has been received).
	@objc private func connectionTimeout()
	{
		NSLog( "Connection timeout. Cancelling" )
		if let peripheral = _peripheral {
			_centralManager?.cancelPeripheralConnection( peripheral )
		}
		disableButtons()
	}
	
	// MARK: - Private functions
	
	private func disableButtons()
	{
		buttonImage.setImageNamed( "icn_mute_watch" )
		buttonGroup.setBackgroundColor( #colorLiteral(red: 0.9098039269, green: 0.4784313738, blue: 0.6431372762, alpha: 1) )
	}
	
	private func enableButtons()
	{
		buttonImage.setImageNamed( "icn_mute_watch" )
		buttonGroup.setBackgroundColor( #colorLiteral(red: 0.721568644, green: 0.8862745166, blue: 0.5921568871, alpha: 1) )
	}
	
	private func startScan()
	{
		if let centralManager = _centralManager, _scanTimeoutTimer == nil
		{
			NSLog( "Starting scan" )
			// Timeout timer since scanning doesn't stop by itself
			_scanTimeoutTimer = Timer.scheduledTimer( timeInterval: 10, target: self, selector: #selector( scanTimeout ), userInfo: nil, repeats: false )
			
			// Scan for devices with remote service
			let remoteService = CBUUID( string: Config.GCACServiceUUID )
			centralManager.scanForPeripherals( withServices: [ remoteService ], options: nil )
			
			// Animate progress
			buttonGroup.setBackgroundColor( #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1) )
			buttonImage.setImageNamed( "Activity" )
			buttonImage.startAnimatingWithImages(in: NSMakeRange( 0, 15 ), duration: 1, repeatCount: 0 )
		}
	}
	
	private func stopScan()
	{
		buttonImage.setImageNamed( "icn_mute_watch" )
	}
	
	private func attemptReconnect()
	{
		// See here for reconnect procedure: https://developer.apple.com/library/prerelease/content/documentation/NetworkingInternetWeb/Conceptual/CoreBluetooth_concepts/BestPracticesForInteractingWithARemotePeripheralDevice/BestPracticesForInteractingWithARemotePeripheralDevice.html#//apple_ref/doc/uid/TP40013257-CH6-SW9
		
		// Try to find a known peripheral
		if let preferredDeviceUUIDString = _preferredDeviceUUIDString, let deviceUUID = UUID( uuidString: preferredDeviceUUIDString ), let centralManager = _centralManager {
			let knownPeripherals = centralManager.retrievePeripherals( withIdentifiers: [ deviceUUID ] )
			if let peripheral = knownPeripherals.first {
				NSLog( "Retrieved known peripheral. Re-connecting…" )
				_peripheral = peripheral	// Need to keep a strong reference in order to connect
				centralManager.connect( peripheral, options: nil )
				return
			}
		}
		
		// Next, try to find a connected device with the GCAC service
		let connectedPeripherals = _centralManager?.retrieveConnectedPeripherals( withServices: [CBUUID( string: Config.GCACServiceUUID )] )
		NSLog( "Connected peripherals: \(String(describing: connectedPeripherals))" )
		if let peripheral = connectedPeripherals?.first {
			NSLog( "Retrieved connected peripheral with GCAC service. Connecting…" )
			_peripheral = peripheral	// Need to keep a strong reference in order to connect
			_centralManager?.connect( peripheral, options: nil )
			return
		}
		
		// Lastly, scan and discover normally
		NSLog( "Starting scan…" )
		startScan()
	}

	/**
	Sends the specified command to the command characteristic.
	
	:param: command	The command number to send.
	*/
	func sendCommand( _ command: Int )
	{
		guard let peripheral = _peripheral, let commandCharacteristic = _commandCharacteristic else {
			NSLog( "Not correctly connected." )
			return
		}
		
		// Build command string
		let commandString = NSString( format: "S-%03d", command )
		peripheral.writeValue( commandString.data( using: String.Encoding.utf8.rawValue )!, for: commandCharacteristic, type:.withResponse )
	}
	
	private func startConnectionTimeout()
	{
		// Start new X second timer
		_connectionTimer?.invalidate()
		_connectionTimer = Timer.scheduledTimer( timeInterval: 4, target: self, selector: #selector(connectionTimeout), userInfo: nil, repeats: false )
	}
	
	private func sendVolumeCommand(_ direction: VolumeDirection )
	{
		WKInterfaceDevice.current().play( .click )

		if direction == .up {
			print( "Up" )
			sendCommand( 2 )
		} else {
			print( "Down" )
			sendCommand( 5 )
		}
	}
	
	// MARK: - WKCrownDelegate functions
	
	func crownDidRotate(_ crownSequencer: WKCrownSequencer?, rotationalDelta: Double )
	{
		// Reset accumulated rotation if direction has changed
		if (_rotation > 0 && rotationalDelta < 0) || (_rotation < 0 && rotationalDelta > 0) {
			_rotation = 0
		}
		
		// Accumulate and check if we're over threshold
		_rotation += rotationalDelta

		if abs( _rotation ) >= 0.15 {
			sendVolumeCommand( (_rotation > 0 ? .up : .down) )
			_rotation = 0
		}
	}
	
	// MARK: - CBCentralManagerDelegate methods
	
	func centralManagerDidUpdateState( _ central: CBCentralManager )
	{
		NSLog( "CB state now: \(String( describing: central.state ))" )
		
		switch central.state
		{
		case .poweredOn:
			// Powered on: try to connect
			if _peripheral == nil {
				NSLog( "Not currently connected: attempting reconnect" )
				attemptReconnect()
			} else {
				NSLog( "Already connected to \(_peripheral!.identifier)" )
			}
			
		default:
			NSLog( "CB state: \(central.state)" )
		}
	}
	
	func centralManager( _ central: CBCentralManager, willRestoreState dict: [String : Any] )
	{
		NSLog( "Will restore state: \(dict)" )
	}
	
	func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber )
	{
		NSLog( "Found device \(peripheral.identifier.uuidString), name: \(peripheral.name ?? "?"), advertisement: \(advertisementData)" )
		
		// Connect to first found device and stop scanning
		_scanTimeoutTimer?.invalidate()
		_scanTimeoutTimer = nil
		_centralManager?.stopScan()
		
		// Connect
		_peripheral = peripheral
		_centralManager?.connect( peripheral, options: nil )
	}
	
	func centralManager( _ central: CBCentralManager!, didRetrieveConnectedPeripherals peripherals: [AnyObject]! )
	{
		NSLog( "Retrieved connected peripherals: \(String(describing: peripherals))" )
	}
	
	func centralManager( _ central: CBCentralManager, didConnect peripheral: CBPeripheral )
	{
		NSLog( "Connected. Discovering services" )
		_peripheral = peripheral

		peripheral.delegate = self
		peripheral.discoverServices( [CBUUID( string: Config.GCACServiceUUID )])
		//		bluetoothIconImageView.tintColor = kBluetoothOnTintColor
		
		_sharedDefaults?.set( peripheral.identifier.uuidString, forKey: Config.defaultsKey_PreferredDeviceUUID )
		
		// Stop connection timeout timer
		_connectionTimer?.invalidate()
		_connectionTimer = nil
	}
	
	func centralManager( _ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error? )
	{
		NSLog( "Disconnected peripheral" )
		//		bluetoothIconImageView.tintColor = kBluetoothOffTintColor
	}
	
	func centralManager( _ central: CBCentralManager, didFailToConnect peripheral: CBPeripheral, error: Error? )
	{
		NSLog( "Failed to connect. Error: \(String(describing: error))" )
		
		// Disable buttons
		disableButtons()
	}
	
	// MARK: - CBPeripheralDelegate methods
	
	func peripheral( _ peripheral: CBPeripheral, didDiscoverServices error: Error? )
	{
		NSLog( "Discovered services: \(String(describing: peripheral.services))" )
		
		if let services = peripheral.services {
			for service in services {
				if service.uuid.uuidString == Config.GCACServiceUUID {
					NSLog( "Discovering characteristics" )
					_service = service
					peripheral.discoverCharacteristics( [CBUUID( string: Config.GCACCommandCharacteristic )], for: service )
					break
				}
			}
		}
	}
	
	func peripheral( _ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error? )
	{
		NSLog( "Discovered characteristics for GCAC service: \(String(describing: peripheral.services))" )
		
		// Check for errors
		if error != nil {
			NSLog( "Error discovering characteristics: \(String(describing: error))" )
			disableButtons()
			return
		}
		
		// Make sure it's the GCAC service
		if service == _service
		{
			// Find the command characteristic
			if let characteristics = service.characteristics {
				for characteristic in characteristics
				{
					if characteristic.uuid.uuidString == Config.GCACCommandCharacteristic {
						NSLog( "Found GCAC command characteristic" )
						_commandCharacteristic = characteristic
						
						// We're ready
						enableButtons()
					}
				}
			}
		}
	}
	
	func peripheral( _ peripheral: CBPeripheral, didWriteValueFor characteristic: CBCharacteristic, error: Error? )
	{
		// Error?
		if error != nil {
			NSLog( "Error writing value: \(String(describing: error))" )
			return
		}
		
		NSLog( "Wrote value" )
	}

}
