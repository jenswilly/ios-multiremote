//
//  Config.swift
//  Bluetooth LE
//
//  Created by Jens Willy Johannsen on 21/09/2017.
//  Copyright © 2017 Greener Pastures. All rights reserved.
//

import UIKit

struct Config
{
	static let sharedDefaultsSuiteName = "group.dk.greenerpastures.bluetoothRemote"
	static let coreBluetoothRestoreIdentifier = "MultiRemoteCentralManager"
	static let defaultsKey_PreferredDeviceUUID = "kDefaultsKey_PreferredDeviceUUID"
	static let defaultsKey_AutoConnect = "kDefaultsKey_AutoConnect"
	static let GCACServiceUUID = "B8B96269-562A-408F-8155-0B45F21C7774"
	static let GCACCommandCharacteristic = "BF33AEAF-8653-4841-89C8-330FA4F13346"
	static let GCACResponseCharacteristic = "51494780-28c9-4502-87f1-C23881C70300"
	static let userDefaults_PreferredDeviceKey = "kUserDefaults_PreferredDeviceKey"
	static let scanTimeout: TimeInterval = 5
}

/**
Objective-C interface for config constants.
Expose any constants that are used by Obj-C here.

Include the compatibility header `Bluetooth_LE-Swift.h` and access as `Constants.xxx`.
*/
@objc class Constants: NSObject
{
	private override init() {}
	
	@objc class func userDefaults_PreferredDeviceKey() -> String { return Config.userDefaults_PreferredDeviceKey }
	@objc class func GCACServiceUUID() -> String { return Config.GCACServiceUUID }
	@objc class func GCACCommandCharacteristic() -> String { return Config.GCACCommandCharacteristic }
	@objc class func scanTimeout() -> TimeInterval { return Config.scanTimeout }
}
