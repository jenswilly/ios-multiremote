//
//  TodayViewController.swift
//  TodayExtension
//
//  Created by Jens Willy Johannsen on 03/10/14.
//  Copyright (c) 2014 Greener Pastures. All rights reserved.
//

import UIKit
import NotificationCenter
import CoreBluetooth

let kBluetoothOffTintColor = UIColor.darkGray
let kBluetoothOnTintColor = UIColor.white	// UIColor( red: 67/255, green: 233/255, blue: 170/255, alpha: 1 )

class TodayViewController: UIViewController, NCWidgetProviding, CBCentralManagerDelegate, CBPeripheralDelegate
{
	@IBOutlet var _commandButtons: [UIButton]!

	private var _centralManager: CBCentralManager? = nil
	private var _preferredDeviceUUIDString: String?
	private var _scanTimeoutTimer: Timer?
	private var _peripheral: CBPeripheral?
	private var _service: CBService?
	private var _commandCharacteristic: CBCharacteristic?

	/**
	Keeps track of whether the widget is currently visible. Is updated in `viewWillAppear()` and `viewWillDisappear()`.

	When the widget is requested to update, `viewWillAppear` will be called which will initiate the BLE connection process.

	But since `viewWillDisappear` is called _before_ the BLE manager is properly initialized, `centralManager.cancelPeripheralConnection` is not called and the connection process continues even though the widget is no longer visible.

	So the flow becomes:

	1. viewWillAppear
	2. BLE connection start
	3. viewWillDisappear
	4. BLE manager ready
	5. BLE connect to device

	This variable is used to keep track of view appear/disappear so BLE callbacks can verify whether the widget is actually visible.
	*/
	private var _isVisible: Bool = false

	deinit
	{
		NSLog( "Widget deinit()" )
		// BLE cleanup
		if let peripheral = _peripheral, let centralManager = _centralManager {
			NSLog( "Disconnecting from \(peripheral) in extension deinit()" )
			centralManager.cancelPeripheralConnection( peripheral )
			_peripheral = nil
		}
	}

    override func viewDidLoad()
	{
        super.viewDidLoad()

		// Load preferred device UUID string if we have it
		let sharedDefaults = UserDefaults( suiteName: Config.sharedDefaultsSuiteName )
		_preferredDeviceUUIDString = sharedDefaults?.object( forKey: Config.defaultsKey_PreferredDeviceUUID ) as! String?

		// Handle iOS 10 heights
		preferredContentSize = CGSize( width: 0, height: 320 )
		extensionContext?.widgetLargestAvailableDisplayMode = .expanded
    }
	
	override func viewWillAppear(_ animated: Bool )
	{
		_isVisible = true	// We're visible
		super.viewWillAppear( animated )
		
		// Power up BLE
		_centralManager = CBCentralManager( delegate: self, queue: nil, options: [CBCentralManagerOptionRestoreIdentifierKey: Config.coreBluetoothRestoreIdentifier] )
		NSLog( "Preferred UUID: \(_preferredDeviceUUIDString ?? "?")" )
		
		// Disable all buttons
		disableButtons()
	}
	
	override func viewWillDisappear(_ animated: Bool )
	{
		_isVisible = false // No longer visible
		super.viewWillDisappear( animated )
		
		// Disconnect. We could have put this in deinit() as well since this extension instance is always deallocated before being shown again.
		// So viewWillAppear/viewWillDisppear is pretty much the same as viewDidLoad/deinit. Except viewWillDisappear will be called quicker when just hiding the Today Extensions.
		if let peripheral = _peripheral, let centralManager = _centralManager {
			NSLog( "Disconnecting from \(peripheral) in extension viewWillDisappear()" )
			centralManager.cancelPeripheralConnection( peripheral )
			_peripheral = nil
		}
	}
	
	func widgetActiveDisplayModeDidChange(_ activeDisplayMode: NCWidgetDisplayMode, withMaximumSize maxSize: CGSize )
	{
		switch activeDisplayMode {
		case .compact:
			preferredContentSize = CGSize( width: 0, height: 110 )
			
		case .expanded:
			preferredContentSize = CGSize( width: 0, height: 320 )

		@unknown default:
			preferredContentSize = CGSize( width: 0, height: 320 )
		}
	}
	
    func widgetPerformUpdate( completionHandler: (@escaping (NCUpdateResult) -> Void) )
	{
		NSLog( "widget perform update" )
        // Perform any setup necessary in order to update the view.

        completionHandler(NCUpdateResult.newData)
    }

	func startScan()
	{
		// Return immediately if we're not visible any longer
		if !_isVisible {
			NSLog( "Widget not visible in startScan() - exiting" )
			return
		}

		if let centralManager = _centralManager
		{
			NSLog( "Starting scan" )
			// Timeout timer since scanning doesn't stop by itself
			_scanTimeoutTimer = Timer.scheduledTimer( timeInterval: 10, target: self, selector: #selector( TodayViewController.scanTimeout ), userInfo: nil, repeats: false )

			// Scan for devices with remote service
			let remoteService = CBUUID( string: Config.GCACServiceUUID )
			centralManager.scanForPeripherals( withServices: [ remoteService ], options: nil )
		}
	}

	@objc func scanTimeout()
	{
		NSLog( "Scan timeout. State currently: \(String(describing: _centralManager?.state.rawValue))" )
		_centralManager?.stopScan()
	}

	/**
	Enables all command buttons in the IBOutletCollection _commandButtons.
	*/
	func enableButtons()
	{
		// Disable all buttons
		for button in _commandButtons {
			button.isEnabled = true
		}
	}

	/**
	Disables all command buttons in the IBOutletCollection _commandButtons.
	*/
	func disableButtons()
	{
		// Disable all buttons
		for button in _commandButtons {
			button.isEnabled = false
		}
	}

	/**
	Sends the specified command to the command characteristic.

	:param: command	The command number to send.
	*/
	func sendCommand( _ command: Int )
	{
		guard let peripheral = _peripheral, let commandCharacteristic = _commandCharacteristic else {
			NSLog( "Not correctly connected." )
			return
		}

		// Build command string
		let commandString = NSString( format: "S-%03d", command )
		peripheral.writeValue( commandString.data( using: String.Encoding.utf8.rawValue )!, for: commandCharacteristic, type:.withResponse )
	}

	func attemptReconnect()
	{
		// Return immediately if we're not visible any longer
		if !_isVisible {
			NSLog( "Widget not visible in attemptReconnect() - exiting" )
			return
		}

		// See here for reconnect procedure: https://developer.apple.com/library/prerelease/content/documentation/NetworkingInternetWeb/Conceptual/CoreBluetooth_concepts/BestPracticesForInteractingWithARemotePeripheralDevice/BestPracticesForInteractingWithARemotePeripheralDevice.html#//apple_ref/doc/uid/TP40013257-CH6-SW9
		
		// Try to find a known peripheral
		if let preferredDeviceUUIDString = _preferredDeviceUUIDString, let deviceUUID = UUID( uuidString: preferredDeviceUUIDString ), let centralManager = _centralManager {
			let knownPeripherals = centralManager.retrievePeripherals( withIdentifiers: [ deviceUUID ])
			if let peripheral = knownPeripherals.first, _isVisible {
				NSLog( "Retrieved known peripheral. Re-connecting…" )
				_peripheral = peripheral
				centralManager.connect( peripheral, options: nil )
				return
			}
		}
		
		// Next, try to find a connected device with the GCAC service
		let connectedPeripherals = _centralManager?.retrieveConnectedPeripherals( withServices: [CBUUID( string: Config.GCACServiceUUID )] )
		NSLog( "Connected peripherals: \(String(describing: connectedPeripherals))" )
		if let peripheral = connectedPeripherals?.first, _isVisible {
			NSLog( "Retrieved connected peripheral with GCAC service. Connecting…" )
			_peripheral = peripheral
			_centralManager?.connect( _peripheral!, options: nil )	// Just assigned _peripheral = peripheral, so unwrap is ok
			return
		}

		// Lastly, scan and discover normally
		NSLog( "Starting scan…" )
		startScan()
	}

	// MARK: IBActions

	@IBAction func commandButtonAction( _ sender: UIButton )
	{
		// The button's tag is the command value
		sendCommand( sender.tag )
	}

	@IBAction func disconnectAction( _ sender: AnyObject )
	{
		if let peripheral = _peripheral {
			NSLog( "Disconnecting" )
			_centralManager?.cancelPeripheralConnection( peripheral )
		}
	}

	@IBAction func toggleConnectionAction( _ sender: UIButton )
	{
		guard let peripheral = _peripheral, let centralManager = _centralManager else {
			NSLog( "Not connected" )
			return
		}

		// Are we already connected?
		if peripheral.state == CBPeripheralState.connected {
			// Yes: disconnect
			NSLog( "Currently connected. Disconnecting…" )
			centralManager.cancelPeripheralConnection( peripheral )
		}
		else {
			// No: connect
			centralManager.connect( peripheral, options: nil )
		}
	}

	@IBAction func autoConnectSwitchAction( _ sender: UISwitch )
	{
		// If we're off and connected, disconnect
		if let peripheral = _peripheral, let centralManager = _centralManager, peripheral.state == .connected && !sender.isOn {
			NSLog( "Disconnecting..." )
			centralManager.cancelPeripheralConnection( peripheral )
		}


		// If switch is on, connect
		if let centralManager = _centralManager, centralManager.state == .poweredOn && sender.isOn {
			attemptReconnect()
		}

		// Save value in user defaults
		let sharedDefaults = UserDefaults( suiteName: Config.sharedDefaultsSuiteName )
		sharedDefaults?.set( sender.isOn, forKey: Config.defaultsKey_AutoConnect )
	}

	// MARK: - CBCentralManagerDelegate methods

	func centralManagerDidUpdateState( _ central: CBCentralManager )
	{
		NSLog( "CB state now: \(central.state.rawValue)" )

		// Return immediately if we're not visible any longer
		if !_isVisible {
			NSLog( "Widget not visible in centralManagerDidUpdateState() - exiting" )
			return
		}

		switch central.state
		{
		case .poweredOn:
			// Powered on: try to connect
			if _peripheral == nil {
				NSLog( "Not currently connected: attempting reconnect" )
				attemptReconnect()
			} else {
				NSLog( "Already connected to \(_peripheral!.identifier)" )
			}
			
		default:
			NSLog( "CB state: \(central.state)" )
		}
	}

	func centralManager( _ central: CBCentralManager, willRestoreState dict: [String : Any] )
	{
		NSLog( "Will restore state: \(dict)" )
	}

	func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber )
	{
		// Return immediately if we're not visible any longer
		if !_isVisible {
			NSLog( "Widget not visible in centralManager(didDiscover peripheral:) - exiting" )
			return
		}

		NSLog( "Found device \(peripheral.identifier.uuidString), name: \(peripheral.name ?? "?"), advertisement: \(advertisementData)" )

		// Connect to first found device and stop scanning
		_scanTimeoutTimer?.invalidate()
		_centralManager?.stopScan()

		// Connect
		_peripheral = peripheral
		_centralManager?.connect( peripheral, options: nil )
	}

	func centralManager( _ central: CBCentralManager!, didRetrieveConnectedPeripherals peripherals: [AnyObject]! )
	{
		NSLog( "Retrieved connected peripherals: \(String(describing: peripherals))" )
	}

	func centralManager( _ central: CBCentralManager, didConnect peripheral: CBPeripheral )
	{
		// Return immediately if we're not visible any longer
		if !_isVisible {
			NSLog( "Widget not visible in centralManager(didConnect peripheral:) - exiting" )
			return
		}

		NSLog( "Connected. Discovering services" )
		peripheral.delegate = self
		peripheral.discoverServices( [CBUUID( string: Config.GCACServiceUUID )])
//		bluetoothIconImageView.tintColor = kBluetoothOnTintColor
	}
	
	func centralManager( _ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error? )
	{
		NSLog( "Disconnected peripheral" )

		// Disable all buttons
		disableButtons()

//		bluetoothIconImageView.tintColor = kBluetoothOffTintColor
	}

	func centralManager( _ central: CBCentralManager, didFailToConnect peripheral: CBPeripheral, error: Error? )
	{
		NSLog( "Failed to connect. Error: \(String(describing: error))" )

		// Disable buttons
		disableButtons()
	}

	// MARK: - CBPeripheralDelegate methods

	func peripheral( _ peripheral: CBPeripheral, didDiscoverServices error: Error? )
	{
		// Return immediately if we're not visible any longer
		if !_isVisible {
			NSLog( "Widget not visible in peripheral(didDiscoverServices:) - exiting" )
			return
		}

		NSLog( "Discovered services: \(String(describing: peripheral.services))" )

		if let services = peripheral.services {
			for service in services {
				if service.uuid.uuidString == Config.GCACServiceUUID {
					NSLog( "Discovering characteristics" )
					_service = service
					peripheral.discoverCharacteristics( [CBUUID( string: Config.GCACCommandCharacteristic )], for: service )
					break
				}
			}
		}
	}

	func peripheral( _ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error? )
	{
		// Return immediately if we're not visible any longer
		if !_isVisible {
			NSLog( "Widget not visible in peripheral(didDiscoverCharacteristicsFor service:) - exiting" )
			return
		}

		NSLog( "Discovered characteristics for GCAC service: \(String(describing: peripheral.services))" )

		// Check for errors
		if error != nil {
			NSLog( "Error discovering characteristics: \(String(describing: error))" )
			disableButtons()
			return
		}

		// Make sure it's the GCAC service
		if service == _service
		{
			// Find the command characteristic
			if let characteristics = service.characteristics {
				for characteristic in characteristics
				{
					if characteristic.uuid.uuidString == Config.GCACCommandCharacteristic {
						NSLog( "Found GCAC command characteristic" )
						_commandCharacteristic = characteristic

						enableButtons()
					}
				}
			}
		}
	}

	func peripheral( _ peripheral: CBPeripheral, didWriteValueFor characteristic: CBCharacteristic, error: Error? )
	{
		// Error?
		if error != nil {
			NSLog( "Error writing value: \(String(describing: error))" )
			return
		}

		NSLog( "Wrote value" )
	}

}
